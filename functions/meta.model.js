var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var metaSchema = new Schema({
    userId: {
        type: String,
        required: true,
        trim: true
    },
    FCMTOKEN: {
        type: String,
        required: true,
        trim: true
    },
    InstallDateTime: {
        type: String,
        required: true,
        trim: true
    },
    NoOfFriends: {
        type: String,
        required: true,
        trim: true
    },
    NoOfInvite: {
        type: String,
        required: true,
        trim: true
    },
    Version: {
        type: String,
        required: true,
        trim: true
    },
    country: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    imei_number: {
        type: String,
        required: true,
        trim: true
    },
    isAudioSent: {
        type: String,
        required: true,
        trim: true
    },
    isDocument: {
        type: String,
        required: true,
        trim: true
    },
    isGroupAdmin: {
        type: String,
        required: true,
        trim: true
    },
    isMoodUpdated: {
        type: String,
        required: true,
        trim: true
    },
    isPhoto: {
        type: String,
        required: true,
        trim: true
    },
    isPing: {
        type: String,
        required: true,
        trim: true
    },
    isSticker: {
        type: String,
        required: true,
        trim: true
    },
    isText: {
        type: String,
        required: true,
        trim: true
    },
    isVideoSent: {
        type: String,
        required: true,
        trim: true
    },
    mood_type: {
        type: String,
        required: true,
        trim: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    "name-lowercase": {
        type: String,
        required: true,
        trim: true
    },
    phone: {
        type: String,
        required: true,
        trim: true
    },
    pictureURL: {
        type: String,
        required: true,
        trim: true
    },
    region: {
        type: String,
        required: true,
        trim: true
    },
    signUpDate: {
        type: String,
        required: true,
        trim: true
    },
    size: {
        type: String,
        required: true,
        trim: true
    },
    thumbnail: {
        type: String,
        required: true,
        trim: true
    },
    update_time: {
        type: String,
        required: true,
        trim: true
    },
    user_mood: {
        type: String,
        required: true,
        trim: true
    }
});


module.exports = mongoose.model('MetaModel', metaSchema, 'MetaModel');
