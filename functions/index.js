const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

const util = require('util');
const FirebaseContactsModel = require('./firebase.contacts.model');
const MetaModel = require('./meta.model');
const mongofunc = require('./mongofunc');

exports.helloWorld = functions.https.onRequest((req, res) => {
  // For Firebase Hosting URIs, use req.headers['fastly-client-ip']
  // For callable functions, use rawRequest
  // Some users have better success with req.headers['x-appengine-user-ip']
  const body = req.body;
  const ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  const headers = JSON.stringify(req.headers, null, 2);
  const message = util.format("<pre>Hello world!\n\nYour IP address: %s\n\nRequest headers: %s</pre>", ipAddress, JSON.stringify(body));
  res.send(message);
});

exports.saveToProdDB = functions.database.ref('/prod/users/{pushid}')
  .onCreate((snapshot, context) => {
    var contactids = Object.keys(snapshot.val().contacts);
    var obj = contactids.map(contact => {
      return new FirebaseContactsModel({
        userId: snapshot.key,
        contactId: contact,
        phone: snapshot.val().meta.phone,
        addedOn: new Date()
      })
    })
    mongofunc.saveToFirebaseContacts(obj);
    return null;
  });

exports.saveToMongo = functions.database.ref('/prod/users/{id}')
  .onCreate((snapshot, context) => {
    var obj = new MetaModel(snapshot.val().meta);
    obj['userId'] = snapshot.key;
    mongofunc.mongoSave(obj);
    return null;
  });


exports.updateToMongo = functions.database.ref('/prod/users/{id}/meta/{change}')
  .onUpdate((change, context) => {
    if (change.before.exists()) {
      var obj = {};
      obj[change.after.key] = change.after.val();
      mongofunc.mongoUpdate(context.params.id, obj);
    }
    return null;
  });








// exports.deleteFromMongo = functions.database.ref('/prod/users/{id}')
//   .onDelete((snapshot, context) => {
//     console.log(snapshot.val());
//     console.log(snapshot.key);
//     var obj = {};
//     obj[snapshot.key] = snapshot.val();
//     console.log(obj);
//     mongo.connect(url, {
//       useNewUrlParser: true,
//       useUnifiedTopology: true
//     }, (err, client) => {
//       if (err) {
//         console.error(err)
//         return
//       }
//       else {
//         db = client.db('firebase');
//         db.collection('demo').deleteOne(obj, (err, result) => {
//           if (err) {
//             console.log(err);
//           } else {
//             console.log("Success!");
//           }
//           client.close();
//         })
//       }
//     });
//     return null;
//   });


