var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ContactModel = new Schema({
    userId: {
        type: String,
    },
    contactId: {
        type: String,
    },
    phone: {
        type: String,
    },
    addedOn: {
        type: Date,
        default: Date.now,
    },
});


module.exports = mongoose.model('ContactId', ContactModel, 'ContactId');
