const mongo = require('mongodb').MongoClient;
const url = "mongodb+srv://abhishek:helloapptest@cluster0-16fcf.mongodb.net/test";


module.exports.saveToFirebaseContacts = async function (obj) {
  var client = await mongo.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  await client.db('prodportaldb').collection('firebaseContacts').insertMany(obj);
  client.close();
}

module.exports.mongoSave = async function (obj) {
  var client = await mongo.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  await client.db('firebase').collection('demo').insertOne(obj);
  client.close();
}

module.exports.mongoUpdate = async function (id, obj) {
  var client = await mongo.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  await client.db('firebase').collection('demo').updateOne({ userId: id }, { $set: obj });
  client.close();
}